package kamilsaitov.testwarefly;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    String RECIPE_NAME = "Фраппучино";

    private ArrayList<String> products = new ArrayList<>();
    private ArrayList<String> amounts = new ArrayList<>();
    private ArrayList<Integer> cookingSteps = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // no title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getSupportActionBar().hide();
        setContentView(R.layout.activity_main);

        // set top image to be product image
        ImageView recipeImg = findViewById(R.id.recipe_img);
        recipeImg.setImageResource(R.drawable.frappucino);
        recipeImg.setScaleType(ImageView.ScaleType.CENTER_CROP);

        // set recipe name
        TextView recipeName = findViewById(R.id.recipe_name);
        recipeName.setText(String.format("• %s •", RECIPE_NAME));

        // ArrayLists for the RecyclerView
        initRecycleArrays();
        initRecyclerView();

        initHorizontalScrollArray();
        initHorizontalScrollView();

        // button for adding to the product list
        final Button add_to_favlist = findViewById(R.id.add_to_favlist_button);
        add_to_favlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add_to_favlist();
            }
        });

    }

    private void initRecycleArrays(){
        products.add("Молоко");
        amounts.add("200 мл");

        products.add("Взбитые сливки");
        amounts.add("1 ст.л.");

        products.add("Карамельный сироп");
        amounts.add("3 ч.л.");

        products.add("Кофе Jacobs Monarch");
        amounts.add("1 чашечка");

        products.add("Лёд");
        amounts.add("2-3 кубика");
    }

    private void initRecyclerView(){
        RecyclerView recyclerView = findViewById(R.id.ingredients_recycler);
        IngredientRecycleViewAdapter adapter = new IngredientRecycleViewAdapter(products, amounts, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initHorizontalScrollArray() {
        cookingSteps.add(R.drawable.step1);
        cookingSteps.add(R.drawable.step2);
        cookingSteps.add(R.drawable.step3);
        cookingSteps.add(R.drawable.step4);
    }

    private void initHorizontalScrollView(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView = findViewById(R.id.recycle_cooking);
        recyclerView.setLayoutManager(layoutManager);
        cookingRecycleViewAdapter adapter = new cookingRecycleViewAdapter(this, cookingSteps);
        recyclerView.setAdapter(adapter);
    }

    private void add_to_favlist(){
        Toast toast = Toast.makeText(this, "Добавлено в список покупок!", Toast.LENGTH_SHORT);
        toast.show();
    }
}
