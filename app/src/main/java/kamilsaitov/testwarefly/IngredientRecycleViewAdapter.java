package kamilsaitov.testwarefly;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class IngredientRecycleViewAdapter extends RecyclerView.Adapter<IngredientRecycleViewAdapter.ViewHolder>{
    private ArrayList<String> entries = new ArrayList<>();
    private ArrayList<String> amounts = new ArrayList<>();
    private Context context;

    public IngredientRecycleViewAdapter(ArrayList<String> entries, ArrayList<String> amounts, Context context) {
        this.entries = entries;
        this.amounts = amounts;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_ingredients, parent, false);
        ViewHolder viewHolder = new ViewHolder((view));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.entry.setText(entries.get(position));
        holder.amount.setText(amounts.get(position));
    }

    @Override
    public int getItemCount() {
        return entries.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView entry;
        TextView amount;
        ConstraintLayout recLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            entry = itemView.findViewById(R.id.entry);
            amount = itemView.findViewById(R.id.amount);
            recLayout = itemView.findViewById(R.id.rec_layout);

        }
    }
}
