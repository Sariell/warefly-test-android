package kamilsaitov.testwarefly;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

public class cookingRecycleViewAdapter extends RecyclerView.Adapter<cookingRecycleViewAdapter.ViewHolder>{
    private Context context;
    private ArrayList<Integer> images;


    public cookingRecycleViewAdapter(Context context, ArrayList<Integer> images) {
        this.context = context;
        this.images = images;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_cooking, parent, false);
        return new ViewHolder((view));

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.img.setImageResource(images.get(position));
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView img;
        ConstraintLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.cooking_img);
            layout = itemView.findViewById(R.id.rec_layout_cooking);
        }
    }
}
